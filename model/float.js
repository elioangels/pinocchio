/** @file Ready made column definitions for float data. */
"use strict"
const { rePurpose } = require("../liar/ijusthelp")
const { floatField } = require("./primitive")

let rePurposeFloat = (name, min, max, precision) => {
  if (!precision) precision = min
  return rePurpose(floatField, {
    name: name,
    min: min,
    max: max,
    precision: precision,
  })
}

let quantumField = rePurposeFloat("quantumField", 0.00000000001, 0.00000999999)
let fractionalField = rePurposeFloat("fractionalField", 0.000001, 0.999999)
let groceryPriceField = rePurposeFloat("groceryPriceField", 0.5, 10.0, 0.01)
let consumerPriceField = rePurposeFloat("consumerPriceField", 9.99, 99.99, 0.01)
let gadgetPriceField = rePurposeFloat("gadgetPriceField", 89.99, 1999.99, 0.01)
let carPriceField = rePurposeFloat("carPriceField", 8000.0, 80000.99, 1)
let housePriceField = rePurposeFloat("housePriceField", 160000.0, 1000000.99, 1)
let nationalGDPField = rePurposeFloat(
  "nationalGDPField",
  1000000000000.0,
  9999999999999.99,
  1
)

module.exports = {
  quantumField: quantumField,
  fractionalField: fractionalField,
  groceryPriceField: groceryPriceField,
  consumerPriceField: consumerPriceField,
  gadgetPriceField: gadgetPriceField,
  carPriceField: carPriceField,
  housePriceField: housePriceField,
  nationalGDPField: nationalGDPField,
}

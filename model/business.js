/** @file Ready made column definitions for business data. */
"use strict"
const ITransform = require("../liar/itransform")
const { rePurpose } = require("../liar/ijusthelp")
const { atSymbol, spaceField } = require("./common")
const { latinWordField } = require("./gossip")
const {
  address1Field,
  address2Field,
  localityField,
  postCodeField,
} = require("./location")
const { businessTypeField, geoLocationField, jobTitleField } = require("./raw")

let employeeNumberField = {
  name: "employeeNumberField",
  class: "pk",
  calc: [val => val + 1000],
}

let companyNameField = {
  name: "companyNameField",
  class: "concat",
  from: {
    latinWord: rePurpose(latinWordField, {
      min: 1,
      max: 2,
    }),
    spaceField,
    businessTypeSometimes: rePurpose(businessTypeField, {
      transform: [ITransform.funky.splutter(30)],
    }),
  },
  transform: [ITransform.title, ITransform.trim],
}

let departmentField = {
  name: "departmentField",
  class: "quick",
  list: [
    "Sales",
    "Accounts",
    "Marketing",
    "Development",
    "Facilities",
    "Transport",
    "HR",
    "Warehouse",
    "Logitics",
  ],
}

let companySlugField = {
  name: "companySlugField",
  class: "field",
  field: "companyNameField",
  transform: [ITransform.lower, ITransform.slugify],
}

let companyDomainField = {
  name: "companyDomainField",
  class: "concat",
  from: {
    companySlugField,
    randomTLD: {
      class: "marak",
      mustache: ".{{internet.domainSuffix}}",
    },
  },
}

let companyEmailField = {
  name: "companyEmailField",
  class: "concat",
  from: {
    dept: { class: "quick", list: ["info", "contact", "web", "sales"] },
    atSymbol,
    companyDomain: { class: "field", field: "companyDomainField" },
  },
  transform: [ITransform.lower],
}

let companyWebField = {
  name: "companyWebField",
  class: "concat",
  from: {
    http: { class: "exact", value: "https://" },
    wwwSometimes: {
      class: "exact",
      value: "www.",
      transform: [ITransform.funky.splutter(30)],
    },
    companyDomain: { class: "field", field: "companyDomainField" },
  },
  transform: [ITransform.lower],
}

let companyPhoneField = {
  name: "companyPhoneField",
  class: "marak",
  mustache: "{{phone.phoneNumber}}",
}

let companyFaxField = {
  name: "companyFaxField",
  class: "marak",
  mustache: "{{phone.phoneNumber}}",
}

let companyAddress1Field = rePurpose(address1Field, {
  name: "companyAddress1Field",
})
let companyAddress2Field = rePurpose(address2Field, {
  name: "companyAddress2Field",
})
let companyLocalityField = rePurpose(localityField, {
  name: "companyLocalityField",
})
let companyGeoLocationField = rePurpose(geoLocationField, {
  name: "companyGeoLocationField",
})
let companyPostCodeField = rePurpose(postCodeField, {
  name: "companyPostCodeField",
})

module.exports = {
  employeeNumberField: employeeNumberField,
  companyNameField: companyNameField,
  companyAddress1Field: companyAddress1Field,
  companyAddress2Field: companyAddress2Field,
  companyLocalityField: companyLocalityField,
  companyGeoLocationField: companyGeoLocationField,
  companyPostCodeField: companyPostCodeField,
  departmentField: departmentField,
  jobTitleField: jobTitleField,
  companySlugField: companySlugField,
  companyDomainField: companyDomainField,
  companyEmailField: companyEmailField,
  companyWebField: companyWebField,
  companyPhoneField: companyPhoneField,
  companyFaxField: companyFaxField,
}

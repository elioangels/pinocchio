/** @file Ready made column definitions for integer data. */
"use strict"
const { rePurpose } = require("../liar/ijusthelp")
const { basePrimitiveField } = require("./primitive")

let rePurposeInt = (name, min, max) => {
  return rePurpose(basePrimitiveField, { name: name, min: min, max: max })
}

let oneOrTwoField = rePurposeInt("oneOrTwoField", 1, 2)
let of3Field = rePurposeInt("of3Field", 1, 3)
let of4Field = rePurposeInt("of4Field", 1, 4)
let of5Field = rePurposeInt("of5Field", 1, 5)
let diceField = rePurposeInt("diceField", 1, 6)
let of7Field = rePurposeInt("of7Field", 1, 7)
let of8Field = rePurposeInt("of8Field", 1, 8)
let of9Field = rePurposeInt("of9Field", 1, 9)
let of10Field = rePurposeInt("of10Field", 1, 10)
let of11Field = rePurposeInt("of11Field", 1, 11)
let dozenField = rePurposeInt("dozenField", 1, 12)
let of20Field = rePurposeInt("of20Field", 1, 10)
let of24Field = rePurposeInt("of24Field", 1, 24)
let of50Field = rePurposeInt("of50Field", 1, 50)
let of100Field = rePurposeInt("of100Field", 1, 100)
let of1000Field = rePurposeInt("of1000Field", 1, 1000)
let of10000Field = rePurposeInt("of10000Field", 1, 10000)
let townPopulationsField = rePurposeInt("townPopulationsField", 2500, 50000)
let cityPopulationsField = rePurposeInt("cityPopulationsField", 50000, 20000000)
let countryPopulationsField = rePurposeInt(
  "countryPopulationsField",
  10000000,
  1500000000
)
let starsOfGalaxyField = rePurposeInt(
  "starsOfGalaxyField",
  300000000000,
  3000000000000
)

module.exports = {
  zeroOrOneField: basePrimitiveField,
  oneOrTwoField: oneOrTwoField,
  of3Field: of3Field,
  of4Field: of4Field,
  of5Field: of5Field,
  diceField: diceField,
  of7Field: of7Field,
  of8Field: of8Field,
  of9Field: of9Field,
  of10Field: of10Field,
  of11Field: of11Field,
  dozenField: dozenField,
  of20Field: of20Field,
  of24Field: of24Field,
  of50Field: of50Field,
  of100Field: of100Field,
  of1000Field: of1000Field,
  of10000Field: of10000Field,
  townPopulationsField: townPopulationsField,
  cityPopulationsField: cityPopulationsField,
  countryPopulationsField: countryPopulationsField,
  starsOfGalaxyField: starsOfGalaxyField,
}

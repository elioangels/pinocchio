/** @file Ready made column definitions for common stuff data. */
"use strict"
const { rePurpose } = require("../liar/ijusthelp")

let emailField = {
  class: "marak",
  mustache: "{{internet.email}}",
}

let pkField = { name: "pk", class: "pk" }
let hashField = { name: "hashField", class: "hash" }
let spaceField = { name: "spaceField", class: "exact", value: " " }
let nbSpaceField = { name: "nbSpaceField", class: "exact", value: "&nbsp" }
let atSymbol = { name: "atSymbol", class: "exact", value: "@" }

let uRLField = {
  class: "marak",
  mustache: "{{internet.url}}",
}

module.exports = {
  emailField: emailField,
  pkField: pkField,
  hashField: hashField,
  spaceField: spaceField,
  nbSpaceField: nbSpaceField,
  atSymbol: atSymbol,
  uRLField: uRLField,
}

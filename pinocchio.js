/** @file Pinocchio is where the magic happens.
 *
 * Using a list of json "column definitions" this class builds one column at a
 * time, zipping them into a full data set.
 */
"use strict"
const _ = require("lodash")
const faker = require("faker")

const IErect = require("./liar/ierect")
const { cloneDeepBreaking, zippity } = require("./liar/ijusthelp")

/** @file Create random data sets. */
module.exports = class Pinocchio {
  constructor(size) {
    this.size = size
  }

  /** @file Get a recordset of random data matching the definition.
   * @param definitionOfALie The entire data definition. e.g.:
   *****************************************************************************
definitionOfALie = {
   fieldName: {
       name: "fieldName",
       class: "<primitive|gossip|raw|exact|quick|toothpaste|marak|pk|hash|choose|concat|field>",
       method: "<of class>",
       <...optionsOfClass>,
   },
   <...more fields>
}
   *****************************************************************************
   * @param seed Same seed ensures the same data for the same definitions.
   */
  lies(definitionOfALie, seed) {
    if (seed) faker.seed(seed)

    if (!_.isPlainObject(definitionOfALie)) {
      throw Error("`definitionOfALie` must be a plain object, e.g. { k: v }.")
    }

    // Because we're reusing the same defintions for different fields, and
    // because the builder amends the defintion during the Pinocchio propose
    // we need to break the Object references between the field defs.
    let refBrokeDefinitionOfALie = cloneDeepBreaking(definitionOfALie)
    // Pass definitionOfALie in twice: Once to be processed in the first
    // recursion of erectColumns, then to maintain a pointer to the
    // parentDefinition (which we need if the class is `field`.)
    refBrokeDefinitionOfALie = this.erectColumns(
      this.size,
      refBrokeDefinitionOfALie,
      refBrokeDefinitionOfALie
    )
    // Interates each field with a `lies` column and zips them into a dataset.
    return zippity(refBrokeDefinitionOfALie)
  }

  /** @file Erect a single column. It is expected that this will be called
  recursively. The column of data is conveniently added to the field
  defintiion in a property called `lies`. */
  erectColumn(size, fieldDef, parentDefinition) {
    if (fieldDef.class === "concat") {
      // Erect all the sub-columns and concat the results.
      fieldDef.lies = IErect.concat(
        this.erectColumns(size, fieldDef.from, parentDefinition),
        fieldDef.with
      )
    } else if (fieldDef.class === "choose") {
      // Erect all the sub-columns and vary the choice between each.
      fieldDef.lies = IErect.choose(
        this.erectColumns(size, fieldDef.between, parentDefinition)
      )
    } else if (fieldDef.class === "engage") {
      // Erect engaged Things.
      for (let [engagedName, engagedDef] of Object.entries(fieldDef)) {
        if (engagedName !== "class" && !_.isEmpty(engagedDef)) {
          fieldDef[engagedName].lies = this.lies(engagedDef)
        }
      }
      // Zip this subset.
      fieldDef.lies = zippity(fieldDef)
    } else if (fieldDef.class === "field") {
      // Error handling
      if (!parentDefinition.hasOwnProperty(fieldDef.field)) {
        throw Error(`Field \`${fieldDef.field}\` cannot be found.`)
      }
      // Use a field from the current defintion.
      let sourceField = parentDefinition[fieldDef.field]
      // If it has not already been built.
      if (!sourceField.lies) {
        // Build it!...
        sourceField.lies = this.erectColumn(
          size,
          sourceField,
          parentDefinition
        ).lies
      }
      fieldDef.lies = sourceField.lies
    } else {
      // Error handling
      if (!IErect.hasOwnProperty(fieldDef.class)) {
        throw Error(`\'IErect\' does not have a \`${fieldDef.class}\` class.`)
      }
      // Erect a column using the definition `class`.
      fieldDef.lies = IErect[fieldDef.class](size, fieldDef)
    }
    // Post creation transform.
    if (fieldDef.transform) {
      for (let transform of fieldDef.transform) {
        fieldDef.lies = fieldDef.lies.map(transform)
      }
    }
    return fieldDef
  }

  /** @file Erect all the columns in a definition. It is expected that this will
  be called recursively.*/
  erectColumns(size, cursedDefintion, parentDefinition) {
    // For each field in this defintionOfALie...
    for (let fieldDef of Object.values(cursedDefintion)) {
      //Erect a column.
      fieldDef = this.erectColumn(size, fieldDef, parentDefinition)
    }
    // Remove any which were only temporary (to be referenced by other fields).
    for (let [fieldName, fieldDef] of Object.entries(cursedDefintion)) {
      if (fieldDef.remove) {
        delete cursedDefintion[fieldName]
      }
    }
    return cursedDefintion
  }
}

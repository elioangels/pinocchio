# Installing pinocchio

- [Prerequisites](./prerequisites)

## npm

Install into your SASS projects.

```
npm install @elioway/pinocchio
yarn add @elioway/pinocchio
```

## Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/elioangels.git
cd elioangels
git clone https://gitlab.com/elioangels/pinocchio.git
```

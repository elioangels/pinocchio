![](https://elioway.gitlab.io/elioangels/pinocchio/elio-pinocchio-logo.png)

> Played like a fiddle, **the elioWay**

# pinocchio ![alpha](https://elioway.gitlab.io/artwork/icon/alpha/favicon.png "alpha")

Create models of faked data, using Marak's **faker**, and more, using model definitions which closely match your project's MVC models.

- [pinocchio Documentation](https://elioway.gitlab.io/elioangels/pinocchio/)

## Installing

```shell
npm i @elioway/pinocchio
```

- [Installing pinocchio](https://elioway.gitlab.io/elioangels/pinocchio/installing.html)

## Seeing is Believing

```shell
node seeing-is-believing.js
```

## Requirements

- [elioangels Prerequisites](https://elioway.gitlab.io/elioangels/installing.html)

## Nutshell

- [elioangels Quickstart](https://elioway.gitlab.io/elioangels/quickstart.html)
- [pinocchio Quickstart](https://elioway.gitlab.io/elioangels/pinocchio/quickstart.html)

# Credits

- [pinocchio Credits](https://elioway.gitlab.io/elioangels/pinocchio/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/elioangels/pinocchio/apple-touch-icon.png)

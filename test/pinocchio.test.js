const faker = require("faker")
const should = require("chai").should()

const gossip = require("../model/gossip")
const { rePurpose } = require("../liar/ijusthelp")

const ITransform = require("../liar/itransform")
const Pinocchio = require("../pinocchio")

describe("liar | Pinocchio", () => {
  it("basically lies", () => {
    let seed = 123
    let numberOfRows = 5
    let pinocchio = new Pinocchio(numberOfRows)
    let lies = pinocchio.lies(
      {
        iAmPrimitive: {
          name: "iAmPrimitive",
          class: "primitive",
          method: "int",
          min: 0,
          max: 1,
        },
        iGossip: {
          name: "iGossip",
          class: "gossip",
          method: "words",
          file: "ainu",
          min: 1,
          max: 3,
        },
      },
      seed
    )
    lies.should.deep.eql([
      { iAmPrimitive: 1, iGossip: "ki esani kat" },
      { iAmPrimitive: 1, iGossip: "yay komo" },
      { iAmPrimitive: 0, iGossip: "erum kiraw rayke" },
      { iAmPrimitive: 0, iGossip: "horari satke ney" },
      { iAmPrimitive: 0, iGossip: "pewrep tumpaorun pakkay" },
    ])
  })
  it("removes columns of lies", () => {
    let seed = 123
    let numberOfRows = 5
    let pinocchio = new Pinocchio(numberOfRows)
    let lies = pinocchio.lies(
      {
        iAmPrimitive: {
          name: "iAmPrimitive",
          class: "primitive",
          method: "int",
          min: 0,
          max: 1,
          remove: true,
        },
        iGossip: {
          name: "iGossip",
          class: "gossip",
          method: "words",
          file: "ainu",
          min: 1,
          max: 3,
        },
      },
      seed
    )
    lies.should.deep.eql([
      { iGossip: "ki esani kat" },
      { iGossip: "yay komo" },
      { iGossip: "erum kiraw rayke" },
      { iGossip: "horari satke ney" },
      { iGossip: "pewrep tumpaorun pakkay" },
    ])
  })
})

describe("liar | Pinocchio | engage", () => {
  it("elioThing engage", () => {
    let seed = 123
    let numberOfRows = 3
    let pinocchio = new Pinocchio(numberOfRows)
    let lies = pinocchio.lies(
      {
        name: {
          name: "name",
          class: "gossip",
          method: "words",
          min: 1,
          max: 1,
          file: "ainu",
        },
        engage: {
          class: "engage",
          Action: {
            actionStatus: {
              class: "quick",
              list: [
                "FailedActionStatus",
                "CompletedActionStatus",
                "ActiveActionStatus",
                "PotentialActionStatus",
              ],
              name: "actionStatus",
            },
            participant: { class: "primitive", method: "int", min: 1, max: 3 },
            startTime: {
              class: "primitive",
              method: "date",
              min: "2000-01-01",
              max: "2040-01-01",
              transform: [ITransform.toISOString],
            },
          },
          MoveAction: {
            toLocation: {
              class: "gossip",
              method: "words",
              min: 1,
              max: 1,
              file: "ainu",
            },
            fromLocation: {
              class: "gossip",
              method: "words",
              min: 1,
              max: 1,
              file: "ainu",
            },
          },
          ArriveAction: {},
        },
      },
      123
    )
    lies.should.eql([
      {
        name: "orowa",
        engage: {
          Action: {
            actionStatus: "FailedActionStatus",
            participant: 1,
            startTime: "2034-08-03T05:21:46.297Z",
          },
          MoveAction: { toLocation: "soy", fromLocation: "mon" },
        },
      },
      {
        name: "inankur",
        engage: {
          Action: {
            actionStatus: "CompletedActionStatus",
            participant: 3,
            startTime: "2018-10-19T00:56:35.469Z",
          },
          MoveAction: { toLocation: "kuru", fromLocation: "uwasurani" },
        },
      },
      {
        name: "esan",
        engage: {
          Action: {
            actionStatus: "ActiveActionStatus",
            participant: 1,
            startTime: "2029-09-02T00:57:38.070Z",
          },
          MoveAction: { toLocation: "eiwanke", fromLocation: "hanke" },
        },
      },
    ])
  })
})

describe("liar | Pinocchio | gossip", () => {
  it("paragraphs length consistent", () => {
    let seed = 123
    let numberOfRows = 20
    let pinocchio = new Pinocchio(numberOfRows)
    let lies = pinocchio.lies({
      disambiguatingDescription: {
        name: "disambiguatingDescription",
        class: "gossip",
        file: "ainu",
        method: "paragraphs",
        min: 2,
        max: 3,
        transform: [
          ITransform.funky.sprinkle({
            punctuate: ["FUCKIT"],
            densage: 30,
          }),
        ],
      },
    })
    for (let lie of lies) {
      lie.disambiguatingDescription.split("\n\n").length.should.be.gte(1)
      lie.disambiguatingDescription.split("\n\n").length.should.be.lte(3)
    }
  })
})

const faker = require("faker")
const should = require("chai").should()

const IAmPrimitive = require("../liar/iamprimitive")

describe("liar | IAmPrimitive | core | int", () => {
  it("creates int lies", () => {
    faker.seed(123)
    IAmPrimitive.int({ max: 5, min: 1 }).should.be.gte(1)
    IAmPrimitive.int({ max: 5, min: 1 }).should.be.lte(5)
    IAmPrimitive.int({ max: -5, min: -1 }).should.be.lte(-1)
    IAmPrimitive.int({ max: -5, min: -1 }).should.be.gte(-5)
    IAmPrimitive.int({ max: 5, min: 1 }).should.be.a("number")
    IAmPrimitive.int({ max: 5, min: 1 }).toString().should.not.contain(".")
    IAmPrimitive.int({ max: 500, min: 100 }).should.eql(321)
  })
})
describe("liar | IAmPrimitive | core | float", () => {
  it("creates float lies", () => {
    faker.seed(123)
    IAmPrimitive.float({ max: 5, min: 1 }).should.be.gte(1)
    IAmPrimitive.float({ max: 5, min: 1 }).should.be.lte(5)
    IAmPrimitive.float({ max: -5, min: -1 }).should.be.lte(-1)
    IAmPrimitive.float({ max: -5, min: -1 }).should.be.gte(-5)
    IAmPrimitive.float({ max: 5, min: 1 }).should.be.a("number")
    IAmPrimitive.float({ max: 500, min: 100 }).toString().should.contain(".")
    IAmPrimitive.float({ max: 500, min: 100 }).should.eql(320.53)
  })
})
describe("liar | IAmPrimitive | core | bool", () => {
  it("creates boo lies", () => {
    faker.seed(123)
    IAmPrimitive.bool().should.be.a("boolean")
    IAmPrimitive.bool().should.be.ok
  })
})
describe("liar | IAmPrimitive | core | uuid", () => {
  it("creates uuid lies", () => {
    faker.seed(123)
    IAmPrimitive.uuid({}).should.have.lengthOf(
      "hhhhhhhh-hhhh-4hhh-nhhh-hhhhhhhhhhhh".length
    )
    IAmPrimitive.uuid({ template: "hn1" }).should.have.lengthOf("hn1".length)
    IAmPrimitive.uuid({ template: "5" }).should.eql("5")
    for (let i; i++; i < 100) {
      IAmPrimitive.uuid({ template: "n" }).should.satisfy(
        x => !["a", "b", "c", "d", "e", "f"].includes(x)
      )
    }
    IAmPrimitive.uuid({ template: "hn1-hn2 hn3:hn4/hn5_hn6" }).should.eql(
      "711-152 b53:664/485_776"
    )
  })
})
describe("liar | IAmPrimitive | core | date", () => {
  it("creates date lies", () => {
    faker.seed(123)
    IAmPrimitive.date({
      min: new Date(2010, 1, 1),
      max: new Date(2010, 1, 2),
    }).should.be.a("date")
    IAmPrimitive.date({
      min: new Date(2010, 1, 1),
      max: new Date(2010, 1, 2),
    }).should.be.gte(new Date(2010, 1, 1))
    IAmPrimitive.date({
      min: new Date(2010, 1, 1),
      max: new Date(2010, 1, 2),
    }).should.be.lte(new Date(2010, 1, 2))
    IAmPrimitive.date({
      min: new Date(1980, 1, 1),
      max: new Date(2080, 1, 2),
    }).should.eql(new Date(Date.parse("2022-12-07T05:53:46.047Z")))
  })
})
describe("liar | IAmPrimitive | core | past", () => {
  it("creates past lies", () => {
    faker.seed(123)
    IAmPrimitive.past({ years: 10 }).should.be.a("date")
    IAmPrimitive.past({ years: 10 }).should.be.lte(new Date())
    IAmPrimitive.past({ years: 1000, ref: "2020-01-01" }).should.eql(
      new Date(Date.parse("1734-01-19T03:23:32.689Z"))
    )
  })
})
describe("liar | IAmPrimitive | core | future", () => {
  it("creates future lies", () => {
    faker.seed(123)
    IAmPrimitive.future({ years: 10 }).should.be.a("date")
    IAmPrimitive.future({ years: 10 }).should.be.gte(new Date())
    IAmPrimitive.future({ years: 1000, ref: "2020-01-01" }).should.eql(
      new Date(Date.parse("2305-12-13T20:36:27.311Z"))
    )
  })
})
describe("liar | IAmPrimitive | core | recent", () => {
  it("creates recent lies", () => {
    faker.seed(123)
    IAmPrimitive.recent({ days: 10 }).should.be.a("date")
    IAmPrimitive.recent({ days: 10 }).should.be.lte(new Date())
    IAmPrimitive.recent({ days: 1000, ref: "2020-01-01" }).should.eql(
      new Date(Date.parse("2019-03-20T20:39:20.419Z"))
    )
  })
})
describe("liar | IAmPrimitive | core | soon", () => {
  it("creates soon lies", () => {
    faker.seed(123)
    IAmPrimitive.soon({ days: 10 }).should.be.a("date")
    IAmPrimitive.soon({ days: 10 }).should.be.gte(new Date())
    IAmPrimitive.soon({ days: 1000, ref: "2020-01-01" }).should.eql(
      new Date(Date.parse("2020-10-13T03:20:39.581Z"))
    )
  })
})
describe("liar | IAmPrimitive | core | time", () => {
  it("creates time lies", () => {
    faker.seed(123)
    IAmPrimitive.time({ format: "unix" }).should.be.a("number")
    IAmPrimitive.time({ format: "wide" }).should.contain("GMT")
    IAmPrimitive.time({ format: "abbr" }).should.have.lengthOf(
      "5:29:25 PM".length
    )
    IAmPrimitive.time({ format: "abbr" }).split(":").should.have.lengthOf(3)
    IAmPrimitive.time({ format: "abbr" }).should.eql("11:08:23 AM")
  })
})

const faker = require("faker")
const should = require("chai").should()

const IJustHelp = require("../liar/ijusthelp")
const marketing = require("../model/marketing")

describe("liar | IJustHelp | cloneDeepBreaking", () => {
  it("rePurpose lies", () => {
    // Behaviour we're breaking:
    let x = { a: 1 }
    let main1 = { A1: x, A2: x }
    main1.should.eql({ A1: { a: 1 }, A2: { a: 1 } })
    x.a = 2
    x.should.eql({ a: 2 })
    // both references to `x.a` change in main1
    main1.should.eql({ A1: { a: 2 }, A2: { a: 2 } })
    main1.A1.a = 3
    // both references to `a` change in main1
    main1.should.eql({ A1: { a: 3 }, A2: { a: 3 } })
    // `x` also changes by reference
    x.should.eql({ a: 3 })
    x = { b: 4 }
    x.should.eql({ b: 4 }) // duh!
    // both references to `x` broken in main1
    main1.should.eql({ A1: { a: 3 }, A2: { a: 3 } })
    main1 = { A1: x, A2: x }
    main1.should.eql({ A1: { b: 4 }, A2: { b: 4 } })
    let main2 = IJustHelp.cloneDeepBreaking(main1)
    // clones exactly
    main2.should.eql({ A1: { b: 4 }, A2: { b: 4 } })
    x.b = 5
    x.should.eql({ b: 5 })
    // only main1's references to `x.b` change.
    main1.should.eql({ A1: { b: 5 }, A2: { b: 5 } })
    // references to `x.b` are broken in main2.
    main2.should.eql({ A1: { b: 4 }, A2: { b: 4 } })
    main2.A1.b = 999
    // only main2's A1 references to `b` change.
    main1.should.eql({ A1: { b: 5 }, A2: { b: 5 } })
    // references between A1 and A2's `x.b` are broken in main2.
    main2.should.eql({ A1: { b: 999 }, A2: { b: 4 } })
    // `x` does not change
    x.should.eql({ b: 5 })
  })
  it("rePurpose marketing model lies", () => {
    let marketingModel = IJustHelp.rePurpose(marketing)
    delete marketingModel.productColorField
    delete marketingModel.postageCostField
    delete marketingModel.productStockedField
    delete marketingModel.productWeightField
    delete marketingModel.tweetField
    delete marketingModel.descriptionField
    delete marketingModel.webPageField
    marketingModel.productNameField.should.deep.eql({
      name: "productNameField",
      class: "raw",
      file: "product",
      field: "product",
    })
  })
})
describe("liar | IJustHelp | freakquency", () => {
  it("freakquent lies", () => {
    IJustHelp.freakquency(["a", "b", "c", "d"]).should.eql(
      [].concat([0, 0, 0, 0]).concat([1, 1, 1]).concat([2, 2]).concat([3])
    )
  })
})
describe("liar | IJustHelp | puncture", () => {
  it("creates puncture lies", () => {
    faker.seed(123)
    IJustHelp.puncture("abcdefghijklmnopqrstuvwxyz".split(""), [
      ",",
      ";",
    ]).should.eql(
      []
        .concat(["a", "b", "c", "d", "e", "f", ","])
        .concat(["g", "h", ",", "i", "j", ",", "k"])
        .concat(["l", ",", "m", "n", ",", "o", "p"])
        .concat([",", "q", "r", ",", "s", "t", ","])
        .concat(["u", "v", ",", "w", "x", "y", "z"])
    )
  })
})
describe("liar | IJustHelp | rePurpose", () => {
  it("rePurpose lies", () => {
    IJustHelp.rePurpose({ a: "A", b: "B" }, { b: "II", c: 3 }).should.deep.eql({
      a: "A",
      b: "II",
      c: 3,
    })
    IJustHelp.rePurpose({ a: "A", b: "B" }).should.deep.eql({ a: "A", b: "B" })
  })
  it("rePurpose marketing model lies", () => {
    let marketingModel = IJustHelp.rePurpose(marketing)
    delete marketingModel.productColorField
    delete marketingModel.postageCostField
    delete marketingModel.productStockedField
    delete marketingModel.productWeightField
    delete marketingModel.tweetField
    delete marketingModel.descriptionField
    delete marketingModel.webPageField
    marketingModel.productNameField.should.deep.eql({
      name: "productNameField",
      class: "raw",
      file: "product",
      field: "product",
    })
  })
})

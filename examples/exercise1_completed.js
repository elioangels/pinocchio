"use strict"
const ITransform = require("../liar/itransform")
const Pinocchio = require("../pinocchio")

let numberOfRows = 20
let pinocchio = new Pinocchio(numberOfRows)

// Make a defintion for random numbers between 1 and 6.
let dice = {
  class: "primitive",
  method: "int",
  min: 1,
  max: 6,
}

// Make a defintion for two dice using the defintion above,
let dice1 = dice
let dice2 = dice

// Define two alternating players.
let player = {
  name: "player",
  class: "toothpaste",
  list: ["white", "black"],
}

let backgammonModel = {
  player: player,
  dice1: dice1,
  dice2: dice2,
}

let lies = pinocchio.lies(backgammonModel)
console.log(lies)

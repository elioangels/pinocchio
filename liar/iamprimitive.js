/** @file Creates realistic, random primitive data */
"use strict"
const _ = require("lodash")
const faker = require("faker")

const uuid = template => {
  let TEMPLATE = template || "hhhhhhhh-hhhh-4hhh-nhhh-hhhhhhhhhhhh"
  var replacePlaceholders = function (placeholder) {
    var value =
      placeholder == "h"
        ? faker.random.number({ min: 0, max: 15 })
        : faker.random.number({ min: 0, max: 9 })
    return value.toString(16)
  }
  return TEMPLATE.replace(/[hn]/g, replacePlaceholders)
}

var time = opts => {
  if (!opts) opts = {}
  if (!opts.hours) opts.hours = { min: 0, max: 23 }
  if (!opts.mins) opts.mins = { min: 0, max: 60 }
  if (!opts.secs) opts.secs = { min: 0, max: 60 }
  if (!opts.milli) opts.milli = { min: 0, max: 1000 }
  var date = new Date(
    2000,
    1,
    1,
    faker.random.number(opts.hours),
    faker.random.number(opts.mins),
    faker.random.number(opts.secs),
    faker.random.number(opts.milli)
  )
  switch (opts.format) {
    case "abbr":
      return date.toLocaleTimeString()
    case "wide":
      return date.toTimeString()
    case "unix":
      return date.getTime()
    case "milli":
      return (
        date.toLocaleTimeString() +
        ":" +
        _.padStart(date.getMilliseconds(), 4, "0")
      )
    default:
      return date.toLocaleTimeString()
  }
}

module.exports = {
  bool: opts => faker.random.boolean(),
  date: opts => faker.date.between(opts.min, opts.max),
  float: opts => faker.random.float(opts),
  future: opts => faker.date.future(opts.years, opts.ref),
  int: opts => faker.random.number(opts),
  past: opts => faker.date.past(opts.years, opts.ref),
  percent: opts => faker.random.number({ min: 0, max: 100 }),
  recent: opts => faker.date.recent(opts.days, opts.ref),
  soon: opts => faker.date.soon(opts.days, opts.ref),
  time: opts => time(opts),
  uuid: opts => uuid(opts.template),
}

"use strict"
const _ = require("lodash")
const faker = require("faker")

const IAmPrimitive = require("../liar/iamprimitive")
const IJustHelp = require("../liar/ijusthelp")

const append = thingName => {
  return val => `${val} ${thingName}`
}

const splutter = percent => {
  return val => (IAmPrimitive.percent() > percent ? val : "")
}

const sprinkle = opts => {
  return val => {
    let sentences = val.split(".")
    let sprinkled = []
    for (let sentence of sentences) {
      let wordList = sentence.split(" ")
      wordList = IJustHelp.puncture(wordList, opts)
      sprinkled.push(wordList.join(" ").replace(/ [\.,;:]/g, c => c.trim()))
    }
    return sprinkled.join(".")
  }
}

// `funky` return functions which accept `val` as a parameter.
const funky = {
  funky: {
    append: append,
    splutter: splutter,
    sprinkle: sprinkle,
  },
}

// Directly accept `val` as a parameter.
module.exports = {
  ...funky,
  capitalize: val => _.capitalize(val),
  chomp: val => val.toString()[0],
  lower: val => _.toLower(val),
  slugify: val => _.kebabCase(val),
  spaceless: val => val.replace(/ /g, ""),
  title: val => _.startCase(val),
  toISOString: val => val.toISOString(),
  trim: val => val.trim(),
  upper: val => _.toUpper(val),
}
